# Oh, hi 🤪

This is a basic wordpress VM for vagrant. Here's how to use it.

1. Set dev server IP address in vagrant config file  
```
subl Vagrantfile
```

1. Set server name in Apache2 config files  
```
subl config-apache2/sites-available/default
```
1. Set DB creedntials in provisioning script  
```
subl provision.sh
```
1. Dont forget to update hosts with your new devserver address  
```
subl /etc/hosts
```
1. Change webapp address in browserSync() (it's in _gulpfile.js_)  
```
subl gulpfile.js
```
1. Untar wordpress files into _www_ directory
1. Install required node modules  
```
npm i -D
```
1. Launch 🚀  
```
npm start
```
1. In your web browser go to _http://localhost:3000_
1. Enjoy ☕️

Of course you should install  _[node.js](https://nodejs.org/en/)_ & [Vagrant](https://www.vagrantup.com) if u want this to work 😁
